package pl.dkrezel.beeralbum.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dkrezel on 2015-05-31.
 */
@DatabaseTable(tableName = "beer_production")
public class BeerProduction {

	@DatabaseField(generatedId = true, columnName = "id")
	private int mID;

	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true)
	private Beer mBeer;

	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true)
	private Production mProduction;

	public int getID() {
		return mID;
	}

	public void setID(int mID) {
		this.mID = mID;
	}

	public Beer getBeer() {
		return mBeer;
	}

	public void setBeer(Beer mBeer) {
		this.mBeer = mBeer;
	}

	public Production getProduction() {
		return mProduction;
	}

	public void setProduction(Production mProduction) {
		this.mProduction = mProduction;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		BeerProduction that = (BeerProduction) o;

		if (mID != that.mID) return false;
		if (!mBeer.equals(that.mBeer)) return false;
		if (!mProduction.equals(that.mProduction)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = mID;
		result = 31 * result + mBeer.hashCode();
		result = 31 * result + mProduction.hashCode();
		return result;
	}
}
