package pl.dkrezel.beeralbum.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dkrezel on 2015-05-30.
 */
@DatabaseTable(tableName = "kind")
public class Kind implements Comparable<Kind> {

	@DatabaseField(generatedId = true, columnName = "id")
	private int mID;

	@DatabaseField(columnName = "name")
	private String mName;

	@ForeignCollectionField()
	private ForeignCollection<BeerKind> mBeerList;

	public Kind() {
	}

	public Kind(String mName) {
		this.mName = mName;
	}

	public int getID() {
		return mID;
	}

	public void setID(int mID) {
		this.mID = mID;
	}

	public String getName() {
		return mName;
	}

	public void setName(String mName) {
		this.mName = mName;
	}

	public ForeignCollection<BeerKind> getBeerList() {
		return mBeerList;
	}

	public void setBeerList(ForeignCollection<BeerKind> mBeerList) {
		this.mBeerList = mBeerList;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Kind kind = (Kind) o;

		if (mID != kind.mID) return false;
		if (!mName.equals(kind.mName)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = mID;
		result = 31 * result + mName.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return this.mName;
	}

	@Override
	public int compareTo(Kind another) {
		return this.mName.compareTo(another.mName);
	}
}
