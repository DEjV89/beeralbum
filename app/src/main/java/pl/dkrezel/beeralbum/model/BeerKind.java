package pl.dkrezel.beeralbum.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dkrezel on 2015-05-31.
 */
@DatabaseTable(tableName = "beer_kind")
public class BeerKind {

	@DatabaseField(generatedId = true, columnName = "id")
	private int mID;

	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true)
	private Beer mBeer;

	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true)
	private Kind mKind;

	public int getID() {
		return mID;
	}

	public void setID(int mID) {
		this.mID = mID;
	}

	public Beer getBeer() {
		return mBeer;
	}

	public void setBeer(Beer mBeer) {
		this.mBeer = mBeer;
	}

	public Kind getKind() {
		return mKind;
	}

	public void setKind(Kind mKind) {
		this.mKind = mKind;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		BeerKind beerKind = (BeerKind) o;

		if (mID != beerKind.mID) return false;
		if (!mKind.equals(beerKind.mKind)) return false;
		if (!mBeer.equals(beerKind.mBeer)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = mID;
		result = 31 * result + mBeer.hashCode();
		result = 31 * result + mKind.hashCode();
		return result;
	}
}
