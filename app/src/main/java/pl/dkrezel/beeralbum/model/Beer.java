package pl.dkrezel.beeralbum.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by dkrezel on 2015-05-30.
 */
@DatabaseTable(tableName = "beer")
public class Beer {

	@DatabaseField(generatedId = true, columnName = "id")
	private int mID;

	@DatabaseField(columnName = "name")
	private String mName;

	@DatabaseField(columnName = "production_date")
	private Date mProductionDate;

	@DatabaseField(columnName = "reminder_date")
	private Date mReminderDate;

	@ForeignCollectionField(eager = true)
	private ForeignCollection<BeerKind> mKindList;

	@ForeignCollectionField(eager = true)
	private ForeignCollection<BeerProduction> mProductionList;

	public Beer() {
	}

	public Beer(String mName, Date mProductionDate, Date mReminderDate) {
		this.mName = mName;
		this.mProductionDate = mProductionDate;
		this.mReminderDate = mReminderDate;
	}

	public int getID() {
		return mID;
	}

	public void setID(int mID) {
		this.mID = mID;
	}

	public String getName() {
		return mName;
	}

	public void setName(String mName) {
		this.mName = mName;
	}

	public Date getProductionDate() {
		return mProductionDate;
	}

	public void setProductionDate(Date mProductionDate) {
		this.mProductionDate = mProductionDate;
	}

	public Date getReminderDate() {
		return mReminderDate;
	}

	public void setReminderDate(Date mReminderDate) {
		this.mReminderDate = mReminderDate;
	}

	public ForeignCollection<BeerKind> getKindList() {
		return mKindList;
	}

	public void setKindList(ForeignCollection<BeerKind> mKindList) {
		this.mKindList = mKindList;
	}

	public ForeignCollection<BeerProduction> getProductionList() {
		return mProductionList;
	}

	public void setProductionList(ForeignCollection<BeerProduction> mProductionList) {
		this.mProductionList = mProductionList;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Beer beer = (Beer) o;

		if (mID != beer.mID) return false;
		if (!mName.equals(beer.mName)) return false;
		if (!mProductionDate.equals(beer.mProductionDate)) return false;
		if (mReminderDate != null ? !mReminderDate.equals(beer.mReminderDate) : beer.mReminderDate != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = mID;
		result = 31 * result + mName.hashCode();
		result = 31 * result + mProductionDate.hashCode();
		result = 31 * result + (mReminderDate != null ? mReminderDate.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Beer ID : ");
		stringBuilder.append(mID);
		stringBuilder.append("\n");
		stringBuilder.append("Beer name : ");
		stringBuilder.append(mName);
		stringBuilder.append("\n");
		stringBuilder.append("Production date : ");
		stringBuilder.append(mProductionDate.toString());
		return stringBuilder.toString();
	}
}
