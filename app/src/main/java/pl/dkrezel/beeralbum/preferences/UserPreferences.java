package pl.dkrezel.beeralbum.preferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by dkrezel on 2015-06-03.
 */
public class UserPreferences {
	private static final String PREFFS_NAME = "user_preferences";

	private static final String KEY_TEXT_SEARCH = "pref.text_search";

	private String mBeerName;

	private SharedPreferences mPreferences;

	public UserPreferences(Context mContext) {
		mPreferences = mContext.getSharedPreferences(PREFFS_NAME, Context.MODE_PRIVATE);

		refresh();
	}

	public UserPreferences refresh() {
		this.mBeerName = mPreferences.getString(KEY_TEXT_SEARCH, "");
		return this;
	}

	public void save() {
		mPreferences.edit()
				.putString(KEY_TEXT_SEARCH, mBeerName)
				.commit();
	}

	public String getBeerName() {
		return mBeerName;
	}

	public UserPreferences setBeerName(String mBeerName) {
		this.mBeerName = mBeerName;
		return this;
	}
}
