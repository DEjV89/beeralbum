package pl.dkrezel.beeralbum.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import pl.dkrezel.beeralbum.R;
import pl.dkrezel.beeralbum.model.Kind;


/**
 * Created by dkrezel on 2015-05-30.
 */
public class KindAdapter extends ArrayAdapter<Kind> {

	private LayoutInflater mInflater;

	public KindAdapter(Context context, List<Kind> mData) {
		super(context, R.layout.spinner_layout, mData);
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		SpinnerHolder mHolder;
		Kind item = getItem(position);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.spinner_layout, parent, false);
			mHolder = new SpinnerHolder();

			TextView mTextView = (TextView) convertView.findViewById(R.id.spinner_text_id);
			mHolder.mTextView = mTextView;

			convertView.setTag(mHolder);
		} else {
			mHolder = (SpinnerHolder) convertView.getTag();
		}

		mHolder.mTextView.setText(item.getName());
		return convertView;
	}

	private static class SpinnerHolder {
		TextView mTextView;
	}
}
