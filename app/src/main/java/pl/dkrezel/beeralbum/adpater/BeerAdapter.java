package pl.dkrezel.beeralbum.adpater;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import pl.dkrezel.beeralbum.R;
import pl.dkrezel.beeralbum.model.Beer;
import pl.dkrezel.beeralbum.model.BeerKind;
import pl.dkrezel.beeralbum.model.BeerProduction;


/**
 * Created by dkrezel on 2015-05-31.
 */
public class BeerAdapter extends ArrayAdapter<Beer> {

	public static final String KEY_POSITION = "KEY_POSITION";
	public static final String KEY_VALUE = "KEY_VALUE";

	private LayoutInflater mInflater;
	private Map<Integer, Boolean> mSelectedBeerMap;
	private List<Beer> mBeerList;
	private List<Beer> mBeerList4Search;

	public BeerAdapter(Context context, List<Beer> data, Bundle mBundle) {
		super(context, R.layout.beer_item, data);
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (mBundle == null) {
			mSelectedBeerMap = new HashMap<>(data.size());
		} else {
			mSelectedBeerMap = new HashMap<>(data.size());
			insertKeyValues(mBundle);
		}
		mBeerList4Search = data;
		mBeerList = new ArrayList<>(data.size());
		mBeerList.addAll(data);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final BeerHolder mHolder;
		final Beer mBeer = getItem(position);
		if (convertView == null) {
			mHolder = new BeerHolder();
			convertView = mInflater.inflate(R.layout.beer_item, parent, false);

			TextView mBeerName = (TextView) convertView.findViewById(R.id.beer_name);
			TextView mBeerKind = (TextView) convertView.findViewById(R.id.beer_kind);
			TextView mBeerProduction = (TextView) convertView.findViewById(R.id.beer_production);
			CheckBox mBeerSelectButton = (CheckBox) convertView.findViewById(R.id.btn_beer_select);

			mHolder.mBeerName = mBeerName;
			mHolder.mBeerKind = mBeerKind;
			mHolder.mBeerProduction = mBeerProduction;
			mHolder.mBeerSelectButton = mBeerSelectButton;
			Boolean mSelectedValue = mSelectedBeerMap.get(position);
			if (mSelectedValue != null) {
				// Je�li istnieje zaznaczona pozycja, to ustaw j� jako aktualn�
				mHolder.mBeerSelectButton.setSelected(mSelectedValue);
			}
			mHolder.mBeerSelectButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					CheckBox mCheckBox = (CheckBox) v;
					int tag = (int) mCheckBox.getTag();
					mSelectedBeerMap.put(tag, mCheckBox.isChecked());
				}
			});

			convertView.setTag(mHolder);
		} else {
			mHolder = (BeerHolder) convertView.getTag();
		}

		mHolder.mBeerName.setText(mBeer.getName());
		StringBuilder stringBuilder = new StringBuilder();
		for (BeerKind kind : mBeer.getKindList()) {
			stringBuilder.append(kind.getKind().getName());
			stringBuilder.append(", ");
		}
		mHolder.mBeerKind.setText(stringBuilder.toString());

		stringBuilder = new StringBuilder();
		for (BeerProduction production : mBeer.getProductionList()) {
			stringBuilder.append(production.getProduction().getName());
			stringBuilder.append(", ");
		}
		mHolder.mBeerProduction.setText(stringBuilder.toString());

		mHolder.mBeerSelectButton.setTag(position);
		if (mSelectedBeerMap.size() == 0) {
			mHolder.mBeerSelectButton.setChecked(false);
		} else {
			if (mSelectedBeerMap.containsKey(position)) {
				mHolder.mBeerSelectButton.setChecked(mSelectedBeerMap.get(position));
			} else {
				mHolder.mBeerSelectButton.setChecked(false);
			}
		}

		return convertView;
	}

	public Map<Integer, Boolean> getSelectedBeerMap() {
		return mSelectedBeerMap;
	}

	public void filter(String stringFilter) {
		stringFilter = stringFilter.toLowerCase(Locale.getDefault());
		mBeerList4Search.clear();
		if (stringFilter.length() == 0) {
			mBeerList4Search.addAll(mBeerList);
		} else {
			for (Beer beer : mBeerList) {
				if (beer.getName().toLowerCase(Locale.getDefault()).contains(stringFilter)) {
					mBeerList4Search.add(beer);
				}
			}
			mSelectedBeerMap.clear();
		}
		notifyDataSetChanged();
	}

	public void refreshBeerList(List<Beer> mBeerList) {
		this.mBeerList.clear();
		this.mBeerList.addAll(mBeerList);
	}

	private void insertKeyValues(Bundle mBundle) {
		int[] mKeyArray = mBundle.getIntArray(BeerAdapter.KEY_POSITION);
		boolean[] mValueArray = mBundle.getBooleanArray(BeerAdapter.KEY_VALUE);
		for (int i = 0; i < mKeyArray.length; i++) {
			mSelectedBeerMap.put(mKeyArray[i], mValueArray[i]);
		}
	}


	private static class BeerHolder {
		TextView mBeerName;
		TextView mBeerKind;
		TextView mBeerProduction;
		CheckBox mBeerSelectButton;
	}

}
