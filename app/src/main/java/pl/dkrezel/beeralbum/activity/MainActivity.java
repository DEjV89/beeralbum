package pl.dkrezel.beeralbum.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;

import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.Locale;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;
import pl.dkrezel.beeralbum.R;
import pl.dkrezel.beeralbum.adpater.BeerAdapter;
import pl.dkrezel.beeralbum.db.BeerOpenHelper;
import pl.dkrezel.beeralbum.model.Beer;
import pl.dkrezel.beeralbum.preferences.UserPreferences;

/**
 * Created by dkrezel on 2015-07-02.
 */
public class MainActivity extends ActionBarActivity {

	public static final String SELECTED_ITEM_ID = "selected.beer.id";

	@InjectView(R.id.beer_list_view)
	protected ListView mBeerListView;

	@InjectView(R.id.search_beer_id)
	protected EditText mSearchBeerName;

	private BeerAdapter mAdapter;

	// Połączenie do DB
	private ConnectionSource mConnection;
	private Dao<Beer, Integer> mBeerDao;

	private UserPreferences mUserPreferences;

	private BeerTextWatcher mBeerTextWatcher;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.beer_main_list);
		ButterKnife.inject(this);
		mBeerTextWatcher = new BeerTextWatcher();

		mConnection = new AndroidConnectionSource(new BeerOpenHelper(this));
		mUserPreferences = new UserPreferences(this);

		if (savedInstanceState == null) {
			readPreferences();
		}

		try {
			mBeerDao = DaoManager.createDao(mConnection, Beer.class);
			mAdapter = new BeerAdapter(this, mBeerDao.queryForAll(), savedInstanceState);
			mBeerListView.setAdapter(mAdapter);
			mSearchBeerName.addTextChangedListener(mBeerTextWatcher);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	protected void readPreferences() {
		mSearchBeerName.setText(mUserPreferences.getBeerName());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (R.id.create_item == item.getItemId()) {
			// Otwórz nową aktywność w celu utworzenia elementu listy
			Intent mIntent = new Intent(this, BeerActivity.class);
			startActivity(mIntent);
		}
		if (R.id.delete_items == item.getItemId()) {
			deleteSelectedItems(mAdapter.getSelectedBeerMap());
		}
		return super.onOptionsItemSelected(item);
	}

	private void deleteSelectedItems(Map<Integer, Boolean> mSelectedBeerMap) {
		if (mSelectedBeerMap.size() > 0) {
			for (Map.Entry<Integer, Boolean> entry : mSelectedBeerMap.entrySet()) {
				if (entry.getValue()) {
					Beer mBeer = mAdapter.getItem(entry.getKey().intValue());
					try {
						mBeerDao.delete(mBeer);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			try {
				// odśwież listę pozycji
				mSelectedBeerMap.clear();
				mAdapter.clear();
				mAdapter.refreshBeerList(mBeerDao.queryForAll());
				mAdapter.addAll(mBeerDao.queryForAll());
				mAdapter.notifyDataSetChanged();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mAdapter.clear();
		try {
			mAdapter.addAll(mBeerDao.queryForAll());
			mAdapter.refreshBeerList(mBeerDao.queryForAll());
			mBeerTextWatcher.afterTextChanged(null);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
		setAndSaveUserPrefs();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			mConnection.close();
		} catch (SQLException e) {
			// Unable to close dataBase connection
			e.printStackTrace();
		}
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@OnItemClick(R.id.beer_list_view)
	public void onItemClick(int position) {
		Beer mSelectedItem = (Beer) mBeerListView.getItemAtPosition(position);

		Intent mIntent = new Intent(this, BeerActivity.class);
		mIntent.putExtra(SELECTED_ITEM_ID, mSelectedItem.getID());
		startActivity(mIntent);
	}

	private class BeerTextWatcher implements TextWatcher {
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {

		}

		@Override
		public void afterTextChanged(Editable s) {
			String mMovieName = mSearchBeerName.getText().toString().toLowerCase(Locale.getDefault());
			mAdapter.filter(mMovieName);
		}
	}

	private void setAndSaveUserPrefs() {
		mUserPreferences.setBeerName(mSearchBeerName.getText().toString());
		mUserPreferences.save();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		int counter = 0;
		int[] checkBoxKeyTab = new int[mAdapter.getSelectedBeerMap().size()];
		boolean[] checkBoxValueTab = new boolean[mAdapter.getSelectedBeerMap().size()];
		for (Map.Entry<Integer, Boolean> entries : mAdapter.getSelectedBeerMap().entrySet()) {
			checkBoxKeyTab[counter] = entries.getKey().intValue();
			checkBoxValueTab[counter] = entries.getValue().booleanValue();
			counter++;
		}
		outState.putIntArray(BeerAdapter.KEY_POSITION, checkBoxKeyTab);
		outState.putBooleanArray(BeerAdapter.KEY_VALUE, checkBoxValueTab);
		super.onSaveInstanceState(outState);
	}
}
