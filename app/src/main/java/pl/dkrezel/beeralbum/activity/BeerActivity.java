package pl.dkrezel.beeralbum.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import pl.dkrezel.beeralbum.R;
import pl.dkrezel.beeralbum.adpater.KindAdapter;
import pl.dkrezel.beeralbum.adpater.ProductionAdapter;
import pl.dkrezel.beeralbum.db.BeerOpenHelper;
import pl.dkrezel.beeralbum.model.Beer;
import pl.dkrezel.beeralbum.model.BeerKind;
import pl.dkrezel.beeralbum.model.BeerProduction;
import pl.dkrezel.beeralbum.model.Kind;
import pl.dkrezel.beeralbum.model.Production;

/**
 * Created by dkrezel on 2015-05-30.
 */
public class BeerActivity extends ActionBarActivity {

	@InjectView(R.id.beer_name)
	protected TextView mTextView;

	@InjectView(R.id.beer_kind)
	protected Spinner mKindSpinner;

	@InjectView(R.id.beer_production)
	protected Spinner mProductionSpinner;

	@InjectView(R.id.btn_save_id)
	protected Button mSaveButton;

	@InjectView(R.id.beer_kind_button_container_id)
	protected LinearLayout mLinearBeerKind;

	private ConnectionSource mConnection;
	private Dao<Kind, Integer> mKindDao;
	private Dao<Production, Integer> mProductionDao;
	private Dao<Beer, Integer> mBeerDao;
	private Dao<BeerKind, Integer> mBeerKindDao;
	private Dao<BeerProduction, Integer> mBeerProductionDao;

	private Beer mBeer;
	private Kind mKind;
	private Production mProduction;
	int[] beerKindIdTab;
	int[] beerProductionIdTab;
	int mSelectedKindId = -1;
	int mSelectedProductionId = -1;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.beer_layout);
		ButterKnife.inject(this);

		mConnection = new AndroidConnectionSource(new BeerOpenHelper(this));

		try {
			mKindDao = DaoManager.createDao(mConnection, Kind.class);
			mProductionDao = DaoManager.createDao(mConnection, Production.class);
			mBeerDao = DaoManager.createDao(mConnection, Beer.class);
			mBeerKindDao = DaoManager.createDao(mConnection, BeerKind.class);
			mBeerProductionDao = DaoManager.createDao(mConnection, BeerProduction.class);

			if (getIntent().hasExtra(MainActivity.SELECTED_ITEM_ID)) {
				int mSelectedBeerId = getIntent().getExtras().getInt(MainActivity.SELECTED_ITEM_ID);
				mBeer = mBeerDao.queryForId(mSelectedBeerId);
				getSupportActionBar().setTitle(R.string.beer_edit_mode);
				refreshBeerItem();
			} else {
				mBeer = new Beer();
				getSupportActionBar().setTitle(R.string.beer_add_mode);
			}

			initiateKindSpinner(mSelectedKindId);
			initiateProductionSpinner(mSelectedProductionId);

			initiateBeerKindPanel();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void refreshBeerItem() {
		int counter = 0;
		mTextView.setText(mBeer.getName());

		beerKindIdTab = new int[mBeer.getKindList().size()];
		for (BeerKind beerKind : mBeer.getKindList()) {
			beerKindIdTab[counter++] = beerKind.getKind().getID();
		}

		counter = 0;
		beerProductionIdTab = new int[mBeer.getProductionList().size()];
		for (BeerProduction beerProduction : mBeer.getProductionList()) {
			beerProductionIdTab[counter++] = beerProduction.getProduction().getID();
		}

		mSelectedKindId = beerKindIdTab[0];
		mSelectedProductionId = beerProductionIdTab[0];
	}

	@OnClick(R.id.btn_save_id)
	public void onSaveButton() {
		mKind = (Kind) mKindSpinner.getSelectedItem();
		mProduction = (Production) mProductionSpinner.getSelectedItem();

		mBeer.setName(mTextView.getText().toString());
		mBeer.setProductionDate(new Date());
		mBeer.setReminderDate(new Date());

		try {
			mBeerDao.createOrUpdate(mBeer);

			// usuń wszystkie poprzednie rekordy
			mBeerKindDao.delete(mBeer.getKindList());

			// Utwórz nowe rekordy
			BeerKind mBeerKind = new BeerKind();
			mBeerKind.setBeer(mBeer);
			mBeerKind.setKind(mKind);

			mBeerKindDao.create(mBeerKind);

			// usuń wszystkie poprzednie rekordy
			mBeerProductionDao.delete(mBeer.getProductionList());

			// Utwórz nowe rekordy
			BeerProduction mBeerProduction = new BeerProduction();
			mBeerProduction.setBeer(mBeer);
			mBeerProduction.setProduction(mProduction);

			mBeerProductionDao.create(mBeerProduction);

			finish();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void initiateKindSpinner(int selectedItemId) throws SQLException {
		List<Kind> kindList = mKindDao.queryForAll();
		Collections.sort(kindList);    // posortuj listę
		KindAdapter mKindAdapter = new KindAdapter(this, kindList);
		mKindSpinner.setAdapter(mKindAdapter);
		if (selectedItemId != -1) {
			for (Kind kind : kindList) {
				if (kind.getID() == selectedItemId) {
					mKindSpinner.setSelection(kindList.indexOf(kind));
				}
			}
		}
	}

	private void initiateProductionSpinner(int selectedItemId) throws SQLException {
		List<Production> productionList = mProductionDao.queryForAll();
		Collections.sort(productionList);
		ProductionAdapter mProductionAdapter = new ProductionAdapter(this, productionList);
		mProductionSpinner.setAdapter(mProductionAdapter);
		if (selectedItemId != -1) {
			for (Production production : productionList) {
				if (production.getID() == selectedItemId) {
					mProductionSpinner.setSelection(productionList.indexOf(production));
				}
			}
		}
	}


	private void initiateBeerKindPanel() {
		try {
			Button mButton;
			List<Kind> mMovieKindList = mKindDao.queryForAll();
			mLinearBeerKind.removeAllViews();
			for (Kind kind : mMovieKindList) {
				mButton = new Button(this);
				mButton.setText(kind.getName());
				mLinearBeerKind.addView(mButton);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
